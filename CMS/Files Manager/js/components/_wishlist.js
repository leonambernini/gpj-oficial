import getVariables from './_variables.js'
import { formatMoney } from './_functions.js'

let getStart = false;
let firstGet = true;
let prodId = null;
let htmlItem = `
<li style="display: none">
    <div class="ad-showcase-product">
        <div class="product-image">
            <a title="{{NAME}}" href="{{URL}}">
                <img src="{{IMAGE}}" width="500" height="500" alt="" id="">
            </a>
        </div>
        <div class="product-name">
            <a title="{{NAME}}" href="{{URL}}">{{NAME}}</a>
        </div>
        <div class="product-price">
            <a title="{{NAME}}" href="{{URL}}">
                <span class="old-price">{{OLDPRICE}}</span>
                <span class="best-price"><strong>{{BESTPRICE}}</strong></span>
                {{INSTALLMENT}}
            </a>
        </div>
        <button class="remove-to-wishlist" data-prod-id="{{ID}}"><i class="far fa-trash-alt"></i> Excluir da Lista</button>
    </div>
</li>`;

const headers = getVariables('headers');
export default class WishList {
    constructor() {
        let self = this;
        setTimeout( function(){
            if( $('#page-product').length ){
                self.getWhislist('', 'product');
            }
            if( $('#page-wishlist').length ){
                self.getWhislist('', 'wishlist');
            }
            self.events();
        }, 1000);
    }
    getWhislist(type, page){
        if( getStart ){
            return false;
        }

        let self = this;
        let email = this.getEmail();

        if( email == null ){
            return false;
        }

        var settings = {
            "async": true,
            "url": "/api/dataentities/WL/search?email="+email+"&_fields=id,email,items",
            "method": "GET",
            "type": "GET",
            "beforeSend": function(){
                getStart = true;
            },
            "headers": headers,
            "processData": false,
        }
        $.ajax(settings).done(function (response) {
            let items = '';
            let entitieId = null;
            
            if( response.length ){
                entitieId = response[0]['id'] || null;
                items = response[0]['items'] || '';

                if( page == 'product' && firstGet ){
                    firstGet = false;
                    if( items.split('|').indexOf($('#___rc-p-id').val()) >= 0 ){
                        $('.add-to-wishlist').hide();
                        $('.remove-to-wishlist').show();
                    }
                }else if ( page == 'wishlist' && type != 'remove' ){
                    self.getProductsVtex(items);
                }
            }

            if( type == 'add' || type == 'remove' ){
                items = items.split('|')
                if( type == 'add' ){
                    if( items.indexOf(prodId) < 0 ){
                        items.push(prodId);
                    }
                }else if( type == 'remove' ){
                    if( items.indexOf(prodId) >= 0 ){
                        items.splice(type.indexOf(prodId), 1);
                    }
                }
                items = items.join('|');
                self.saveList(items, entitieId);
            }
            getStart = false;
        });
    }
    saveList(items, entitieId){
        let email = this.getEmail();

        let data = {
            "email": email,
            "items": items,
        }

        if( entitieId != undefined && entitieId != null && entitieId != '' ){
            data['id'] = entitieId;
        }

        var settings = {
            "async": true,
            "url": "/api/dataentities/WL/documents",
            "method": "PUT",
            "type": "PUT",
            "headers": headers,
            "processData": false,
            "data": JSON.stringify(data)
        }

        $.ajax(settings).done(function (response) {
            if( response != undefined && response != null && response['Id'] != undefined && response['Id'] != null ){
                console.log('OK');
            }else{
                console.log('NOT');
            }
        });
    }
    getProdId($btn){
        let _prodId = null;
        if( $btn.attr('data-prod-id') != undefined && $btn.attr('data-prod-id') != null && $btn.attr('data-prod-id') != '' ){
            _prodId = $btn.attr('data-prod-id');
        }else{
            _prodId = $('#___rc-p-id').val();
        }
        return _prodId;
    }
    getProductsVtex(items){
        items = items.split('|');

        let params = '?';
        for( let x = 0; x < items.length; x++ ){
            if( $.trim(items[x]) != '' ){
                params += ( ( params == '?' ) ? '' : '&' ) + 'fq=productId:' + items[x];
            }
        }

        var settings = {
            "async": true,
            "url": "/api/catalog_system/pub/products/search"+params,
            "method": "GET",
            "type": "GET",
            "headers": headers,
            "processData": false,
        }
        $.ajax(settings).done(function (response) {
            
            if( response.length ){
                $('.wishlist-content .admake-showcase').html('<ul class="wishlist-grid d-flex flex-wrap"></ul>');

                for( let x = 0; x < response.length; x++ ){
                    let prod = response[x];
                    let item = prod['items'][0];
                    let commertialOffer = item['sellers'][0]['commertialOffer'];
                    let installments = commertialOffer['Installments'];
                    let installment = '';

                    if( installments.length ){
                        let number = installments.pop()['NumberOfInstallments'];
                        let value = formatMoney(installments.pop()['Value'], 2, ',', '.');
                        installment = `<span class="installment">ou <strong>${number}x</strong> de <strong>R$ ${value}</strong> sem juros</span>`;
                    }


                    let price = commertialOffer['Price'];
                    let listPrice = commertialOffer['ListPrice'];

                    if( (price == listPrice) || price <= 0 ){
                        price = '';
                    }else{
                        price = 'R$ ' + formatMoney(price, 2, ',', '.');
                    }
                    listPrice = 'R$ ' + formatMoney(listPrice, 2, ',', '.');

                    let $item = $(htmlItem
                        .replace(/{{ID}}/g, prod['productId'])
                        .replace(/{{NAME}}/g, prod['productName'])
                        .replace(/{{URL}}/g, prod['link'])
                        .replace(/{{IMAGE}}/g, item['images'][0]['imageUrl'])
                        .replace(/{{OLDPRICE}}/g, price)
                        .replace(/{{BESTPRICE}}/g, listPrice)
                        .replace(/{{INSTALLMENT}}/g, installment));
                    $('.wishlist-content .admake-showcase ul').append($item);
                    $item.fadeIn();
                }
            }else{
                $('.wishlist-content .admake-showcase').html('<div class="alert alert-secondary" role="alert">Nenhum produto adicionado</div>');
            }
        });
        
    }
    events() {
        const self = this;
        $('.add-to-wishlist').click( function(){
            if( !self.verifyUser() ){
                alert('Você precisa fazer login para adicionar a lista de desejo.');
                return false;
            }

            prodId = self.getProdId($(this));

            if( prodId != undefined && prodId != null && prodId != '' ){
                self.getWhislist('add', 'product');
    
                $(this).hide();
                $('.remove-to-wishlist').show();
            }

            return false;
        });
        $(document).on('click', '.remove-to-wishlist', function(){
            if( !self.verifyUser() ){
                alert('Você precisa fazer login para remover um item da lista de desejo.');
                return false;
            }

            prodId = self.getProdId($(this));

            if( prodId != undefined && prodId != null && prodId != '' ){
                
                if( $('#page-wishlist').length ){
                    self.getWhislist('remove', 'wishlist');
                    $(this).parents('li').fadeOut(function(){
                        $(this).remove();
                    });
                }else{
                    self.getWhislist('remove', 'product');
                    $(this).hide();
                    $('.add-to-wishlist').show();
                }
            }

            return false;
        });
    }
    verifyUser(){
        let isUser = false;
        for( let x = 0; x < dataLayer.length; x++ ){
            if( dataLayer[x]['visitorLoginState'] != undefined && dataLayer[x]['visitorLoginState'] != null && dataLayer[x]['visitorLoginState'] ){
                isUser = true;
            }
        }
        return isUser;
    }
    getEmail(){
        let email = null;
        for( let x = 0; x < dataLayer.length; x++ ){
            if( dataLayer[x]['visitorContactInfo'] != undefined && dataLayer[x]['visitorContactInfo'] != null && dataLayer[x]['visitorContactInfo'].length ){
                email = dataLayer[x]['visitorContactInfo'][0];
            }
        }
        return email;
    }
}

