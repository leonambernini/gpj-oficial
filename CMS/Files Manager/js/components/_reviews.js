import getVariables from './_variables.js'
const _variables = getVariables();

export default class Reviews {
    constructor(element = '[data-admake="reviews"]') {
        this._element = element;
        this._$el = $(element);
        this._config = null;

        if (this._$el.length) {
            let dataConfig = this._$el.attr('data-config');

            if (dataConfig !== undefined && dataConfig !== null && dataConfig !== '') {
                dataConfig = dataConfig.replace(/\'/g, '"');
                this._config = { ..._variables.reviews, ...JSON.parse(dataConfig) };
            } else {
                this._config = { ..._variables.reviews };
            }

            this.controler();
            this.events();
        }
    }
    createStructColumn(self){
        let $opinion = self._$el.find('#opiniao_de_usuario'),
            $colAvaliacao = $('#opiniao_de_usuario .col-avaliacao'), $colResenha;

        if(!$colAvaliacao.length ){
            $opinion.addClass('row justify-content-between');

            $colAvaliacao = $('<div class="col-md-6 col-avaliacao"></div>').append($opinion.children().not('h4'));
            $colResenha = $('<div class="col-md-6 col-resenha"></div>');

            $opinion.append($colAvaliacao);
            $colResenha.append($opinion.find('ul.resenhas'));
            $opinion.append($colResenha);
        }
    }
    createPagination(self){
        const qtyPage = self._config.qtyByPage;

        if(qtyPage > 1){
            let tamList = $(`${self._element} ul.resenhas > .quem > li`).length,
                it      = tamList / qtyPage;
            
            for (let index = 0; index < it; index++) {
                let $html = $('<ul class="items"></ul>');
                
                $html.append($(`${self._element} ul.resenhas > .quem > li`).slice((index * qtyPage), ((qtyPage * (index+1)))).clone());
                $(`${self._element} ul.resenhas > .quem`).append($html);
            }

            $(`${self._element} ul.resenhas > .quem > li`).remove();
        }

        if( $.fn.slick){
            if(!$(`${self._element} ul.resenhas > .quem`).hasClass('slick-slider')){
                $(`${self._element} ul.resenhas > .quem`).slick(_variables.carousel);
            }
        }else{
            console.log('slick not found');
        }
    }
    controler(){
        if(this._config.column){
            this.createStructColumn(this);
        }
        if(this._config.pagination){
            this.createPagination(this);
        }

        var targetNode = document.querySelector('#opiniao_de_usuario');
        var config = {childList: true};
        var callback = () => {
            if(this._config.column){
                this.createStructColumn(this);
            }
            if(this._config.pagination){
                this.createPagination(this);
            }  
        };
        var observer = new MutationObserver(callback);
        observer.observe(targetNode, config);
    }
    events(){
        $('.product-rating').click(function () {
            $("html, body").animate({ scrollTop: ($('#opiniao_de_usuario').offset().top - (($('#header').outerHeight() || 0) + 25)) }, "slow");
            return false;
        });

        // $(document).on('click', '#opcoes-avalie input', (event) => {
        //     // this.controler();
        // });
    }
}