import Forms from '../components/_forms.js'

export default (element = '#page-institutional') => {
    if ($(element).length) {
        new Forms;

        $('.bread-crumb .bread-crumb ul').append(`<li class="last"><strong>${$('.institutional-title > h1').text()}</strong></li>`);
    }
}