import ProductImage from '../components/_product-image.js'
import Shipping from '../components/_shipping.js'
import Reviews from '../components/_reviews.js'
import addRemQty from '../components/_addRemQty.js'
import titleContentExists from '../components/_titleContentExists.js'
import { billetPrice, stringToSlug, formatMoneyCheckout, formatMoney } from '../components/_functions.js'

function showHideQty() {
    if ($('.product-buy .notifyme-form').length && $('.product-buy .notifyme-form').is(':visible')) {
        $('.product-buy').addClass('no-stock');
    } else {
        $('.product-buy').removeClass('no-stock');
    }
}

export default (element = '#page-product') => {

    if ($(element).length) {
        addRemQty();
        // titleContentExists();
        showHideQty();

        new ProductImage;
        new Shipping;
        new Reviews;

        $('.admake-tab-btn').click( function(){
            let $this = $(this);
            let id = $this.attr('href');

            $('.adamke-tab .admake-tab-btn').removeClass('active');
            $('.adamke-tab-content').removeClass('active');
            $('.adamke-tab-content'+id).addClass('active');
            $this.addClass('active');

            return false;
        });

        $('#cep-shipping').blur( function(){
            var $this = $(this);
            var cep = $(this).val();
            if( cep != undefined && cep != null && cep != '' && cep.length == 9 ){
                $.each( $('.seller-shipping'), function(){
                    var items = [{
                        id: vtxctx['skus'],
                        quantity: parseInt($('.quantitySelector input').val()),
                        seller: $(this).attr('data-seller-id')
                    }];
                    var postalCode = cep;
                    var country = 'BRA';

                    $(this).html('<i class="fas fa-circle-notch fa-spin"></i>');
                    vtexjs.checkout.simulateShipping(items, postalCode, country)
                        .done(function(result) {
                            console.log(result)
                            if( result['items'] != undefined && result['items'] != null && result['items'].length ){
                                var item = result['items'][0];
                                var logisticsInfo = result['logisticsInfo'][0]['slas'][0];
                                var $seller = $('.seller-shipping[data-seller-id="'+item['seller']+'"]');
                                if( $seller.length ){
                                    let days = parseInt(logisticsInfo.shippingEstimate);
                                    let textDays = days + ( ( days > 1 ) ? ' dias úteis' : ' dia útil' );
                                    $seller.html(
                                        '<table>'+
                                        '   <tr>' +
                                        '       <th>Entrega</th>' +
                                        '       <td>'+logisticsInfo['name']+'</td>' +
                                        '   </tr>' +
                                        '   <tr>' +
                                        '       <th>Frete</th>' +
                                        '       <td>R$ '+formatMoneyCheckout(logisticsInfo['price'], 2, ',', '.')+'</td>' +
                                        '   </tr>' +
                                        '   <tr>' +
                                        '       <th>Prazo</th>' +
                                        '       <td>'+textDays+'</td>' +
                                        '   </tr>' +
                                        '</table>'
                                    );
                                }
                            }
                        });
                });
            }
        });


        $('#buy-box-float .col-photo').html(`<img src="${$('#image-main').attr('src')}" alt="" width="70" class="img-fluid" />`);
        $('#buy-box-float .col-name').html(`<p>${$('.product-name > .productName').html()}<p/>`);
        $('#buy-box-float .col-price').html($('.product-price-variation .product-price').html());
        
        $(window).scroll( function(){
            if( $('.price-border .no-stock').length && $('#buy-box-float').is(':visble') ){
                $('#buy-box-float').fadeOut();
            }else if( !$('.price-border .no-stock').length ){
                if( $(this).scrollTop() >= $('.product-price-variation').offset().top && !$('#buy-box-float').is(':visible') ) {
                    $('#buy-box-float').fadeIn();
                }else if( $(this).scrollTop() < $('.product-price-variation').offset().top && $('#buy-box-float').is(':visible') ) {
                    $('#buy-box-float').fadeOut();
                }
            }
        });


        if( $('#divCompreJunto table').length && $('.admake-buy-together').length ){
            $('.admake-buy-together').fadeIn();
        }

        // let discountBoleto = function(){
        //     if( $('.col-image .discount').length ) {
        //         let $flag = $('.col-image .discount').find('[class*="boleto"]'),
        //             $best = $('.descricao-preco .skuBestPrice'),
        //             $html = $('.plugin-preco .preco-a-vista');

        //         $('.descricao-preco .valor-dividido.price-installments').before('<div class="product-best-price-discount"></div><div class="best-price-msg">desconto à vista 1X cartão ou boleto</div>');
        //         $('.descricao-preco').after('<div class="new-sku-best-price"><span>Total a prazo </span>' + $best.html() + '</div>');

        //         $('.productPrice .preco-a-vista.price-cash').hide();
        //         $best.hide();
        //         billetPrice($flag, $best, $('.product-best-price-discount'), 1);

        //         $('.productPrice').addClass('active');
        //     };
        // }
        // discountBoleto();

        if ($('.quantitySelector > label > input').length) {
            $('.quantitySelector > label').append(`
                    <div class="actions">
                        <button class="add-qty btn btn-add-remove" parent="#ad-product-buy-button" element=".quantitySelector > label > input" type="add" tabindex="0">+</button>
                        <button class="remove-qty btn btn-add-remove" parent="#ad-product-buy-button" element=".quantitySelector > label > input" type="remove" tabindex="0">-</button>
                    </div>`);
        }

        // events
        $(document).on('change', '.sku-selector', function () {
            showHideQty();
            // discountBoleto();
        });

        if($('#page-product main .product-main .other-payments').length){
            $(document).on('click', '#page-product main .product-main .other-payments', function () {
                $(this).toggleClass('open');
            });
        };

    }
}
