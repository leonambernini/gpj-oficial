/*************** CSS ***************/
import './css/sass/admake-default.scss'
// import './css/sass/admake-checkout.scss'

/*************** JS ***************/

// PLUGINS
import LazyLoad from "vanilla-lazyload";
import './js/plugins/slick.js'
import './js/plugins/jquery.mask.js'
import './js/plugins/QD_infinityScroll.js'

// FUCTIONS
import { observerMutation, reset, accordion, toggleSerachMobile, getCookies, setCookies } from './js/components/_functions.js'
import getVariables from './js/components/_variables.js'

const headers = getVariables('headers');

// COMPONENTS
import carroselDefault from './js/components/_carrosel.js'
import Cart from './js/components/_cart.js'
import Navbar from './js/components/_navbar.js'
import Newsletter from './js/components/_newsletter.js'
import showcase from './js/components/_showcase'
import WishList from './js/components/_wishlist'

// // PAGES
import Institucional from './js/pages/_institucional.js'
import Department from './js/pages/_department.js'
import Product from './js/pages/_product.js'
import Account from './js/pages/_account.js'
import Home from './js/pages/_home.js'
import CreateYourPc from './js/pages/_create-your-pc.js'

function init() {
    observerMutation('.ui-autocomplete', function () {
        $('.ui-autocomplete li img').each(function () {
            var $this = $(this), href = $this.attr('src').replace('25-25', '90-90');
            $this.attr('src', href);
        });
    });

    // $.ajax({
    //     url: '/no-cache/profileSystem/getProfile',
    //     type: 'GET',
    //     success: function (dataProfile) {
    //         if (dataProfile.IsUserDefined) {
    //             $('.col-header-login').addClass('logged');
    //             if (dataProfile.FirstName) {
    //                 $('.col-header-login button > span').text(dataProfile.FirstName);
    //             } else {
    //                 $('.col-header-login button > span').text(dataProfile.Email);
    //             }
    //         } else {
    //             $('.col-header-login').removeClass('logged');
    //         }
    //     },
    //     error: function () {
    //         callback.erroAjaxcallback('construtor');
    //     },
    //     complete: function () {
    //         $('.col-header-login .content').show();
    //     }
    // });

    // $('#btn-open-search').on('click', function () {
    //     $('#header-main > .container > .row').toggleClass('active');
    // });
}

$(document).ready(function () {

    var lazyLoadInstance = new LazyLoad({
        elements_selector: ".lazy"
    });

    // newsletter
    if ($('#modal-newsletter').length) {    
        setTimeout(function(){
            if(!getCookies('newsletter-modal') || getCookies('newsletter-modal') == 'false'){
                setCookies('newsletter-modal', 'false');
                $('#modal-newsletter').fadeIn();
            }else{
                $('#modal-newsletter').hide();
            }
        }, 5000);
    };

    // newsletter
    $('#modal-newsletter .btn-close-modal, #modal-newsletter .mask, #modal-newsletter .close-popup-news').on('click', function () {
        $('#modal-newsletter').fadeOut();
        setCookies('newsletter-modal', 'true');
    });

    reset();
    init();
    carroselDefault();
    accordion();
    showcase();
    toggleSerachMobile();

    // /** PAGES */
    Home();
    Institucional();
    Department();
    Product();
    Account();
    CreateYourPc();
    /** PAGES */

    new Navbar;
    new Cart;
    new Newsletter;
    new WishList;

    // let bannerIsOpen = localStorage.getItem('admake-banner-popup') || false;

    // if( bannerIsOpen == false ){
    //     localStorage.setItem('admake-banner-popup', true);
    //     if( $('.admake-banner-popup img').length ){
    //         $('.admake-banner-popup').fadeIn();
    //         $('.admake-banner-popup .mask').click( function(){
    //             $('.admake-banner-popup').fadeOut();
    //             return false;
    //         });
    //     }
    // }

    let $qtyCart = $('.link-item-cart .badge');
    if( $qtyCart.length ){
        vtexjs.checkout.getOrderForm().done(function(orderForm) {
            let total = 0;
            for( let x = 0; x < orderForm.items.length; x++ ){
                let item = orderForm.items[x];
                total += item['quantity'];
            }

            $qtyCart.html(total);
        });
    }

    let contactSend = false;
    $('#send-form-contact').click( function(){
        let $this = $(this);
        let btnText = $this.text();

        let $contactMesage = $('#contact-form-mesage');
        if( !$contactMesage.length ){
            $this.before('<div id="contact-form-mesage" class="flex-grow-1 mr-3"></div>');
            $contactMesage = $('#contact-form-mesage');
        }

        let department = $('#ct-department').val();
        let order = ( $('#ct-order').val() != undefined && $('#ct-order').val() != null ) ? $('#ct-order').val() : '';
        let name = $('#ct-name').val();
        let email = $('#ct-email').val();
        let subject = $('#ct-subject').val();
        let mesage = $('#ct-mesage').val();

        console.log(department)
        let msgWarning = '<div class="alert alert-warning" role="alert">Todos os campos com * são obrigátorios: <b>INFORME {msg}</b></div>';
        if( department == undefined || department == null || department == '' ){
            $contactMesage.html(msgWarning.replace('{msg}', 'DEPARTAMENTO'));
            return false;
        }
        if( name == undefined || name == null || name == '' ){
            $contactMesage.html(msgWarning.replace('{msg}', 'O NOME'));
            return false;
        }
        if( email == undefined || email == null || email == '' ){
            $contactMesage.html(msgWarning.replace('{msg}', 'O E-MAIL'));
            return false;
        }
        if( subject == undefined || subject == null || subject == '' ){
            $contactMesage.html(msgWarning.replace('{msg}', 'O ASSUNTO'));
            return false;
        }
        if( mesage == undefined || mesage == null || mesage == '' ){
            $contactMesage.html(msgWarning.replace('{msg}', 'A MENSAGEM'));
            return false;
        }

        if( !contactSend ){
            contactSend = true;
            let data = {
                "department": department,
                "order": order,
                "name": name,
                "email": email,
                "subject": subject,
                "mesage": mesage,
            }

            var settings = {
                "async": true,
                "url": "/api/dataentities/CT/documents",
                "method": "POST",
                "type": "POST",
                "headers": headers,
                "processData": false,
                "beforeSend": function(){
                    $this.html('<i class="fas fa-circle-notch fa-spin"></i>');
                },
                "data": JSON.stringify(data)
            }

            $.ajax(settings).done(function (response) {
                if( response != undefined && response != null && response['Id'] != undefined && response['Id'] != null ){
                    $contactMesage.html('<div class="alert alert-success" role="alert">Recebemos seu contato, entraremos em contato o mais rápido possivel.</div>');
                    $this.html('ENVIADO <i class="fas fa-check-double"></i>');

                    $.each( $('.contact-content input'), function(){
                        $(this).val('');
                    });
                }else{
                    $contactMesage.html('<div class="alert alert-danger" role="alert">Algo deu errado em nosso sistema, tente novamente.</div>');
                    $this.html(btnText);
                }
            });
        }
        return false;
    });

    $.each( $('.admake-showcase'), function(){
        let $this = $(this);
        let $h2 = $this.find('>h2');
        let title = $h2.html();

        if( !$h2.length ){
            return false;
        }

        if( title.indexOf('||') >= 0 ){
            title = title.split('||');
            let title_text = $.trim(title[0]);
            let title_timer = $.trim(title[1]);

            $h2.html(title_text);
            let $div = $(`
                <div class="count-down d-flex justify-content-center">
                    <div class="days"><span></span><small>DIAS</small></div>
                    <div class="hours"><span></span><small>HORAS</small></div>
                    :
                    <div class="minutes"><span></span><small>MIN.</small></div>
                    :
                    <div class="seconds"><span></span><small>SEG.</small></div>
                </div>`);
            $h2.after($div);

            var countDownDate = new Date(title_timer).getTime();

            // Update the count down every 1 second
            var x = setInterval(function() {

                // Get today's date and time
                var now = new Date().getTime();

                // Find the distance between now and the count down date
                var distance = countDownDate - now;

                // Time calculations for days, hours, minutes and seconds
                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                // Display the result in the element with id="demo"
                $div.find('.days span').html(days);
                $div.find('.hours span').html(hours);
                $div.find('.minutes span').html(minutes);
                $div.find('.seconds span').html(seconds);

                // If the count down is finished, write some text
                if (distance < 0) {
                    clearInterval(x);
                    $div.fadeOut();
                }
            }, 1000);
            
        }else if( title.indexOf('|') >= 0 ){
            title = title.split('|');
            $h2.html($.trim(title[0]) + ' <strong>' + $.trim(title[1]) + '</strong>');
        }
    });

    // $('#header-nav .header-nav-item-all .header-nav-sub-item').mouseover( function(){
    //     let $this = $(this);
    //     let $banner = $this.find('.sub-banner img');
    //     let $box = $this.parents('.header-nav-sub-box');
    //     $box.find('>img').remove();
    //     if( $banner.length ){
    //         $box.append($banner.clone())
    //     }
    //     return false;
    // });

    if( $('.benefits-banner').length ){
        $('.benefits-banner ul').slick({
            dots: false,
            arrows: false,
            autoplay: true,
            infinite: true,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 4,
            responsive: [{
                    breakpoint: 1199,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4,
                    }
                },{
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },{
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        variableWidth: true
                    }
                }
            ]
        });
    }

    if( $('#home-brands .brands-carousel').length ){
        $('#home-brands .brands-carousel').slick({
            dots: true,
            autoplay: true,
            infinite: true,
            speed: 300,
            slidesToShow: 5,
            slidesToScroll: 1,
            prevArrow: '<button type="button" class="slick-prev btn"><i class="fas fa-angle-left"></i></button>',
            nextArrow: '<button type="button" class="slick-next btn"><i class="fas fa-angle-right"></i></button>',
            responsive: [{
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 4,
                            slidesToScroll: 4
                        }
                    },{
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },{
                    breakpoint: 768,
                    settings: {
                        dots: false,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        variableWidth: true
                    }
                }
            ]
        });
    }

    if( $('#monte-seu-pc ul').length ){
        $('#monte-seu-pc ul').slick({
            dots: false,
            arrows: false,
            autoplay: true,
            infinite: true,
            speed: 300,
            slidesToShow: 10,
            slidesToScroll: 1,
            responsive: [{
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 1,
                    }
                },{
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },{
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }

    $('.adamke-tabs-list .admake-tab-btn').click( function(){
        var $this = $(this);
        var $btnList = $this.parents('.adamke-tabs-list');
        var $btns = $btnList.find('.admake-tab-btn');
        var id = $this.attr('href');
        if( $(id).length ){
            $btns.removeClass('active');
            $this.addClass('active');

            var $element = $(id);
            var $groupElements = $element.parents('.adamke-tabs-content-box');
            var $contents = $groupElements.find('.adamke-tabs-content');

            $contents.hide();
            $element.slideDown();
            return false;
        }
    });
    $.each( $('.adamke-tabs-list'), function(){
        var $btn = $(this).find('.admake-tab-btn').eq(0);

        if( $btn.length ){
            $btn.click();
        }
    });
});