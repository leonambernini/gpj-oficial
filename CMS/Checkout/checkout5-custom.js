var loader = '<i class="fas fa-sync fa-spin"></i>', orderId,
    prevArrow = '<button type="button" class="slick-prev btn"><i class="icon-angle-left"></i></button>',
    nextArrow = '<button type="button" class="slick-next btn"><i class="icon-angle-right"></i></i></button>',
    header = {
        "Content-Type": "application/json",
        "x-vtex-api-appKey": "vtexappkey-dacris-UKIVYI",
        "x-vtex-api-appToken": "VCGFOKGZNADJWRYWQAVRAQWFTJOXLSNECHSEIMQSJGEMSJLLVIMIYQVEFBLEWINVMZHPYUNDJCHULKMJXDBCGFFMLNYHALWASJZTCWMBUIYWIBTVOCHSGKOHUVELWGCY",
    };

function customData(id, fields) {
    for (const key in fields) {
        if (fields.hasOwnProperty(key)) {
            const element = fields[key];

            $.ajax({
                url: '//dacris.vtexcommercestable.com.br/api/checkout/pub/orderForm/' + orderId + '/customData/' + id + '/' + key,
                type: "PUT",
                headers: header,
                async: false,
                dataType: 'json',
                processData: false,
                data: JSON.stringify({
                    "expectedOrderFormSections":
                        [
                            "items",
                            "gifts",
                            "totalizers",
                            "clientProfileData",
                            "shippingData",
                            "paymentData",
                            "sellers",
                            "messages",
                            "marketingData",
                            "clientPreferencesData",
                            "storePreferencesData",
                            "customData"
                        ],
                    "value": element
                }),
                success: function (data) {
                    console.log(data);
                },
            });
        }
    }
}


$(document).ready(function () {

    $.getScript('//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js');

    var idCluster = $('#footer-showcase').attr('data-cluster'),
        qtdProd = $('#footer-showcase').attr('data-qty'),
        layout = 'a7a4f4b6-486f-47f8-a7b6-01ccc61d906c';

    vtexjs.checkout.getOrderForm().done(function (orderForm) {
        orderId = orderForm.orderFormId;
    });

    $.ajax({
        "url": '/buscapagina?fq=fq=C:/' + idCluster + '&PS=' + qtdProd + '&sl=' + layout + '&cc=21&sm=0&PageNumber=1',
        "method": "GET",
        "headers": {
            "Content-Type": "application/json",
        },
        success: function (data) {
            $('#footer-showcase > .container').append(data);
            $('.helperComplement').remove();
            $('.admake-showcase > ul').slick({
                dots: false,
                arrows: true,
                infinite: false,
                prevArrow: prevArrow,
                nextArrow: nextArrow,
                slidesToShow: 4,
                slidesToScroll: 4,
                responsive: [
                    {
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 576,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });

        },
        error: function (data, status) {
            console.log('errors' + status);
        }
    });

    /** events  */
    $(document).on('click', '#is-corporate-client, #edit-profile-data', function () {
        if (!$('.client-company-bank').length) {
            $('.corporate-info-box').append('<p class="client-company-bank input text required">' +
                '<label for="client-company-bank">Banc\u0103</label>' +
                '<input type="text" id="client-company-bank" class="input-xlarge">' +
                '</p>' +
                '<p class="client-company-iban input text required">' +
                '<label for="client-company-iban">Cont IBAN</label>' +
                '<input type="text" id="client-company-iban" class="input-xlarge">' +
                '</p>');
        }
    });
    $(document).on('click', '#go-to-shipping, #go-to-payment', function () {
        var bank = $('#client-company-bank').val(),
            iban = $('#client-company-iban').val(),
            obj = {};

        if (bank != '') {
            obj['companyBankName'] = bank;
        }
        if (iban != '') {
            obj['companyIban'] = iban;
        }

        if(!$.isEmptyObject(obj)){
            customData('companyProfile', obj);
        }
    });
    $(document).on('click', '#btn-test', function () {
        var obj = {};

        if(!$('#billing-address .error').length){
            $('#billing-address .AddressField').each(function () {
                var $this = $(this);
                var field = $this.find('> input').attr('data-field');
                var value = $this.find('> input').val();

                obj[field] = value;
            })

            if(!$.isEmptyObject(obj)){
                customData('billingAddress', obj);
            }

            $('#shipping-data p > .btn-go-to-payment').click();
        }
    });
    $(document).on('click', '#orderform-minicart-to-cart', function () {
        $('.admake-showcase > ul').slick('resize');
    });
    $(document).on('focusout', '#billing-address .AddressField input.obg', function () {
        var $this = $(this);
        $this.closest('p').find('.help').remove();

        if($this.val() == ''){
            $this.closest('p').append(`<span class="help error">Acest camp este obligatoriu.</span>`);
            $this.addClass('error');
        }else{
            $this.closest('p').find('.help').remove();
            $this.removeClass('error');
        }
    });

    /** mutation */
    let targetNode = document.querySelector('#shipping-data');
    var callback = function () {
        if(!$('#shipping-data #billing-address').length){
            $('.address-form-placeholder').after($('#billing-address').removeClass('hidden'));
        }

        $('.btn-go-to-payment-wrapper').append('<button class="btn btn-success" id="btn-test">Continua cu plata</button>');
        
    };
    var observer = new MutationObserver(callback);
    observer.observe(targetNode, {childList: true});

});